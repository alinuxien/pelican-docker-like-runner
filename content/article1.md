Title: Petit Bilan du Projet 4
Date: 2020-08-27 15:03
Modified: 2020-08-27 15:03
Category: Blog
Tags: pelican, gitlab, github, ci/cd, letsencrypt
Slug: bilan-projet-4
Authors: Ali Akrour
Summary: Un projet simple mais potentiellement vaste

Bien que le pipeline de CI/CD demandé soit très simple pour la génération et le déploiement en ligne d'un blog à l'aide de Pelican, la préparation de l'environnement de travail a ouvert énormément de possibilités, et de découvertes associés.

En particulier, la gestion de la sécurisation du serveur GitLab local a été très rapide et facile avec un nom de domaine réel et enregistré chez un provider, à l'aide de Let's Encrypt et son intégration dans le package GitLab, mais elle s'est avéré longue et délicate dans le cas d'un nom de domaine local.
En effet, la création d'une autorité locale de certification auto-signé a été nécessaire pour signer le certificat du serveur GitLab local, afin que le navigateur ( ici Chrome ) accepte d'intégrer l'autorité de certification et de considérer notre serveur local comme digne de confiance et sécurisé.
Ensuite, l'ajout d'un Runner a amené ses difficultés aussi concernant la sécurité, en interne sur le serveur cette fois-ci, bloquant l'exécution des jobs.
L'utilisation de Git sur le serveur GitLab local a aussi nécessité des aménagements.
Enfin, monter un serveur de cache ou une registry ont aussi rencontré des difficultés liées a la sécurité, mais n'étant pas absolument nécessaire au fonctionnement du projet, j'ai pu m'en affranchir.

Par ailleurs, sur les conseils de mon mentor Jean-Sébastien Techer que je remercie encore au passage pour sa patience et ses encouragements, j'ai eu l'occasion de gérer l'installation et la configuration de mon instance GitLab à l'aide de Ansible, générant une réutilisabilité très importante pour la suite du parcours.

Enfin, tous ces jonglages entre dépôts locaux et en ligne, avec 3 "projets" en parallèle ( gitlab et 2 versions de Pelican ) m'ont permis d'affiner encore ma compréhension de Git et ont été une belle occasion de mise en pratique.

En conclusion, j'ai appris beaucoup de chose sur Ansible, GitLab, Git, et un peu aussi sur Pelican...


