# Bienvenue sur mon projet Pelican ( version Runner de type Docker )
Il s'agit d'un projet réalisé en Aout 2020 dans le cadre de ma formation "Expert DevOps" chez OpenClassRooms.

## Ca fait quoi ?
Ca transforme des articles écrit en Markdown en blog HTML, grâce à l'Intégration et la Livraison Continues ( CI/CD ) fournies par GitLab.

La transformation est automatique, et le déploiement se fait sur GitHub Pages de façon manuelle, en 1 click.

*NB : nous sommes ici sur la ** version Runner de type Docker ** du projet, ce qui est le cas sur GitLab.com.*

*Si vous préférez utiliser votre propre instance de serveur GitLab, une autre version de ce projet prévue pour cela est [disponible ici](https://gitlab.com/alinuxien/pelican)*
 
## Contenu ?
- Les fichiers `pelicanconf.py` et `publishconf.py` de configuration de Pelican
- Le dossier `content` dans lequel vous déposerez vos articles écrit en MarkDown
- Le fichier `.gitlab-ci.yml` de configuration du pipeline de CI/CD
- Le fichier `.gitignore` pour ne versionner que les articles en Markdown et pas les fichiers générés 
 
## J'ai besoin de quoi ?
- Un compte sur GitLab.com et un projet dédié et **vide**, qui conteniendra vos articles sources en Markdown et les fichiers de configuration de Pelican ( je vous explique cela plus loin ), et configuré pour pouvoir "pusher" depuis votre terminal en ssh ( [plus de détails ici](https://gitlab.com/help/ssh/README#gitlab-and-ssh-keys) )
- Un dépot GitHub dédié à la publication des articles générés, avec GitHub Pages activé sur la branche `master` de ce dépôt
- Un token d'accès HTTPS à votre dépôt GitHub ( [plus de détails ici](https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token) )

## Comment ça s'utilise ?
Tout se passe au départ dans un terminal :

- faites une copie locale de ce dépot :  `git clone https://gitlab.com/alinuxien/pelican-docker-like-runner.git`
- allez dans le dossier téléchargé : `cd pelican-docker-like-runner`
- en premier lieu, éditez le fichier `pelicanconf.py` pour définir l'URL de votre site GitHub Pages ( **pas de slash à la fin** ), et pourquoi pas mettre votre nom d'auteur et le nom du site, tels qu'ils apparaîtront dans le site généré.
- ensuite, ajoutez votre dépôt GitLab à git : `git remote add mongitlab [URL du dépôt]` avec  `URL du depôt` à récupérer en version **ssh** dans votre projet GitLab dédié 
- ( par exemple `git remote add mongitlab git@gitlab.example.com:alinuxien/pelican-docker.git` )
- en option vous pouvez configurer votre nom et votre adresse email pour qu'ils apparaissent dans chaque commit : `git config --global user.name "Prénom Nom"` et `git config --global user.email "nom@domaine.ext"`
- vous pouvez écrire un premier article en Markdown avec un nom au choix et une extension `.md`, et éditer le fichier `index.md` fourni, **mais il est important de toujours conserver un fichier `index.md`** pour le bon fonctionnement de GitHub Pages.
- ajoutez tous les fichiers du dossier à git :  `git add .`
- faites un premier commit : `git commit -m "Initial commit"`
- et enfin, pushez sur le dépot GitLab, en définissant ce dépôt pour les prochains push : `git push -u mongitlab master`

Dans votre navigateur, dans votre dépôt GitLab dédié à Pelican : 

- dans les détails du dépôts, vous devriez retrouver tous les fichiers évoqués
- dans les `Settings` -> `CI/CD` -> `Variables`, vous devez créer 3 variables `GH_TOKEN` ( le token configuré sur GitHub au départ, ** TRES IMPORTANT : cochez `Mask variable` ** ), `GH_USERNAME` ( nom d'utilisateur GitHub ), et `GH_REPO` ( nom court du dépôt GitHub dédié à votre blog )
- dans le menu `CI/CD` -> `pipeline` : relancez le stage `build`, puis manuellement le stage `deploy`
- allez voir votre nouveau et joli blog sur votre GitHub Pages ( il peut y avoir parfois quelques minutes de délais )

Et voilà qui est totalement terminé. 

# Et après ?
A chaque fois que vous écrivez un article en Markdown dans le dossier `content`, vous le commitez et le pushez sur votre GitLab. Le build est automatique, et le déploiement est à faire manuellement.

Si vous préférez à terme avoir un déploiement automatique, c'est très simple : vous éditez le fichier `.gitlab-ci` et vous supprimez la dernière ligne ( `when: manual` )

Bon blog à vous !


